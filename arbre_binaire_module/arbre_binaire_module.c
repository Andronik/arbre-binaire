/*

    Copyright (c) 2019 Andronikos Karkaselis

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/**
 *  \file arbre_binaire_module.c
 *
 *  Fichier d'implémentation du module Python permettant d'interfacer
 *  avec la librairie "arbre_binaire".
 *
 */

#include <Python.h>
#include "arbre_binaire.h"

/**
 * \brief "Wrapper" pour les erreurs du module
 */
PyObject* arbre_binaire_erreur;

/**
 * \brief "Wrapper" pour la fonction 'creer_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'adresse de l'arbre créé
 */
PyObject* wrapper_creer(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* arbre_binaire = NULL;
    PyObject * result;
    int valeur;
    if (!PyArg_ParseTuple(args, "i", &valeur)) {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        arbre_binaire = creer_arbre_binaire(valeur);
        if (arbre_binaire) {
            result = Py_BuildValue("n", arbre_binaire);
        } else {
            PyErr_SetString(arbre_binaire_erreur, 
                            "Ne peut pas créer d'arbre binaire");
            result = NULL;
        }
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'charger_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'adresse de l'arbre créé
 */
PyObject* wrapper_charger(PyObject* self, PyObject* args)
{
    bool erreur = false;
    char* nom_fichier;
    void* arbre_binaire = NULL;
    PyObject* arbre_charge;
    if (!PyArg_ParseTuple(args, "s", &nom_fichier)) {
        erreur = true;
    }
    if (erreur) {
        arbre_charge = NULL;
    } else {
        arbre_binaire = charger_arbre_binaire(nom_fichier);
        if (arbre_binaire) {
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
                detruire_arbre_binaire(arbre_binaire);
                arbre_charge = NULL;
            } else {
                arbre_charge = Py_BuildValue("n", arbre_binaire);
            }
        } else {
            PyErr_SetString(arbre_binaire_erreur, 
                            "Ne peut créer d'arbre binaire.");
            arbre_charge = NULL;
        }
    }
    return arbre_charge;
}

/**
 * \brief "Wrapper" pour la fonction 'detruire_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_detruire(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            detruire_arbre_binaire(arbre_binaire);
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'sauvegarder_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_sauvegarder(PyObject* self, PyObject* args)
{
    bool erreur = false;
    char* nom_fichier;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "ns", &arbre_binaire, &nom_fichier)) {
        if (arbre_binaire) {
            sauvegarder_arbre_binaire(arbre_binaire, nom_fichier);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'nombre_elements_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'élément reçu
 */
PyObject* wrapper_nombre_elements(PyObject* self, PyObject* args) 
{
    bool erreur = false;
    int nombre_element = 0;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            nombre_element = nombre_elements_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        result = Py_BuildValue("i", nombre_element);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'nombre_feuilles_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'élément reçu
 */
PyObject* wrapper_nombre_feuilles(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int nombre_feuille = 0;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            nombre_feuille = nombre_feuilles_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        result = Py_BuildValue("i", nombre_feuille);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'hauteur_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'élément reçu
 */
PyObject* wrapper_hauteur(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int hauteur_arbre = 0;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            hauteur_arbre = hauteur_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        result = Py_BuildValue("i", hauteur_arbre);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'element_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python contenant l'élément reçu
 */
PyObject* wrapper_element(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int element_arbre = 0;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            element_arbre = element_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        result = Py_BuildValue("i", element_arbre);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'modifier_element_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_modifier_element(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int valeur;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "ni", &arbre_binaire, &valeur)) {
        if (arbre_binaire) {
            modifier_element_arbre_binaire(arbre_binaire, valeur);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'contient_element_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_contient_element(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int valeur;
    bool contient_element;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "ni", &arbre_binaire, &valeur)) {
        if (arbre_binaire) {
            contient_element = contient_element_arbre_binaire(arbre_binaire, valeur);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = Py_None;

    } else {
        result = Py_BuildValue("i", contient_element);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'enfant_gauche_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_enfant_gauche(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* enfant_gauche;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            enfant_gauche = enfant_gauche_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = Py_None;
    } else {
        result = Py_BuildValue("n", enfant_gauche);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'creer_enfant_gauche_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_creer_enfant_gauche(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int valeur;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "ni", &arbre_binaire, &valeur)) {
        if (arbre_binaire) {
            creer_enfant_gauche_arbre_binaire(arbre_binaire, valeur);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'retirer_enfant_gauche_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_retirer_enfant_gauche(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            retirer_enfant_gauche_arbre_binaire(arbre_binaire);
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'enfant_droite_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_enfant_droit(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* enfant_droit;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            enfant_droit = enfant_droit_arbre_binaire(arbre_binaire);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = Py_None;
    } else {
        result = Py_BuildValue("n", enfant_droit);
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'creer_enfant_droite_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_creer_enfant_droit(PyObject* self, PyObject* args)
{
    bool erreur = false;
    int valeur;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "ni", &arbre_binaire, &valeur)) {
        if (arbre_binaire) {
            creer_enfant_droit_arbre_binaire(arbre_binaire, valeur);
            if (a_erreur_arbre_binaire(arbre_binaire)) {
                erreur = true;
                PyErr_SetString(arbre_binaire_erreur, 
                                erreur_arbre_binaire(arbre_binaire));
            }
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief "Wrapper" pour la fonction 'retirer_enfant_droite_arbre_binaire'
 *
 * \param self L'objet python représentant le module
 * \param args Conteneur possédant les arguments envoyés à la fonction
 *
 * \return Objet Python 'None' (type NULL de Python)
 */
PyObject* wrapper_retirer_enfant_droit(PyObject* self, PyObject* args)
{
    bool erreur = false;
    void* arbre_binaire = NULL;
    PyObject* result;
    if (PyArg_ParseTuple(args, "n", &arbre_binaire)) {
        if (arbre_binaire) {
            retirer_enfant_droit_arbre_binaire(arbre_binaire);
        } else {
            erreur = true;
        }
    } else {
        erreur = true;
    }
    if (erreur) {
        result = NULL;
    } else {
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/**
 * \brief Liste toutes les fonctions du module
 */
PyMethodDef arbre_binaire_methods[] = {
    {"creer",  wrapper_creer, METH_VARARGS,
     "Créer une arbre binaire"},
    {"charger",  wrapper_charger, METH_VARARGS,
     "Charger un arbre binaire à partir d'un fichier"},
    {"detruire",  wrapper_detruire, METH_VARARGS,
     "Détruire l'arbre binaire"},
    {"sauvegarder",  wrapper_sauvegarder, METH_VARARGS,
     "Sauvegarder l'arbre dans un fichier"},
    {"nombre_elements", wrapper_nombre_elements, METH_VARARGS,
     "Le nombre d'éléments contenus dans l'arbre"},
    {"nombre_feuilles", wrapper_nombre_feuilles, METH_VARARGS,
     "Le nombre de feuilles de l'arbre"},
    {"hauteur", wrapper_hauteur, METH_VARARGS,
     "La hauteur de l'arbre"},
    {"element", wrapper_element, METH_VARARGS,
     "L'élément contenu dans la racine de l'arbre"},
    {"nombre_feuilles", wrapper_nombre_feuilles, METH_VARARGS,
     "Le nombre de feuilles de l'arbre"},
    {"modifier_element", wrapper_modifier_element, METH_VARARGS,
     "Remplace l'élément à la racine de l'arbre par la valeur envoyée"},
    {"contient_element", wrapper_contient_element, METH_VARARGS,
     "Vérifie si l'élément se retrouve dans l'arbre"},
    {"enfant_gauche", wrapper_enfant_gauche, METH_VARARGS,
     "Retourne un pointeur vers le sous-arbre enfant gauche de l'arbre"},
    {"creer_enfant_gauche", wrapper_creer_enfant_gauche, METH_VARARGS,
     "Créer un enfant gauche a l'arbre"},
    {"retirer_enfant_gauche", wrapper_retirer_enfant_gauche, METH_VARARGS,
     "Libère l'enfant gauche de l'arbre"},
    {"enfant_droit", wrapper_enfant_droit, METH_VARARGS,
     "Retourne un pointeur vers le sous-arbre enfant droit de l'arbre"},
    {"creer_enfant_droit", wrapper_creer_enfant_droit, METH_VARARGS,
     "Créer un enfant droit a l'arbre"},
    {"retirer_enfant_droit", wrapper_retirer_enfant_droit, METH_VARARGS,
     "Libère l'enfant droit de l'arbre"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

/**
 * \brief Information sur le module
 */
struct PyModuleDef arbre_binaire_module = {
    PyModuleDef_HEAD_INIT,
    "arbre_binaire_externe", /* nom du module */
    "Arbre binaire implémenté grâce à plusieurs structures liée entre elles",
    -1,
    arbre_binaire_methods
};

/**
 * \brief Initialization du module
 */
PyMODINIT_FUNC PyInit_arbre_binaire_externe(void)
{
    PyObject* module;
    module = PyModule_Create(&arbre_binaire_module);
    if (module) {
        arbre_binaire_erreur=PyErr_NewException("arbre_binaire_externe.erreur", NULL, NULL);
        Py_INCREF(arbre_binaire_erreur);
        PyModule_AddObject(module, "erreur", arbre_binaire_erreur);
    }
    return module;
}
