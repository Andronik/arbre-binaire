/*

    Copyright (c) 2019 Andronikos Karkaselis

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/**
 *  \file arbre_binaire.c
 *
 *  Fichier d'implémentation contenant la structure de donnée 'arbre_binaire'.
 *  Un arbre binaire est un graphe connexe acyclique débutant par une racine 
 *  qui peut avoir jusqu'à maximum deux enfants avec une valeur pour chaque 
 *  "point" de connection. Ces enfants peuvent avoir à leur tour jusqu'à deux
 *  enfants contenant des valeurs et ainsi de suite.
 *
 */

#ifndef _arbre_binaire_h
#define _arbre_binaire_h

#include <stdbool.h>

#define ERREUR_TAILLE 255

/**
 * \brief Un arbre binaire qui contient une valeur et des enfants.
 */
typedef struct arbre_binaire_struct arbre_binaire;

/**
 *  \brief Créé un nouvel arbre binaire.
 *  
 *  \param valeur La valeur à la racine de l'arbre
 *
 *  \return L'arbre binaire
 */
arbre_binaire* creer_arbre_binaire(int valeur);

/**
 *  \brief Créé un nouvel arbre_binaire contenu dans un fichier.
 *
 *  Créé un nouvel arbre_binaire et charge le contenue du fichier
 *  'nom_fichier' dans l'arbre. Si le fichier n'est pas valide, indique
 *  l'erreur dans 'arbre->erreur'.
 *
 *  \param nom_fichier Le nom du fichier contenant l'arbre
 *
 *  \return L'arbre binaire
 *
 *  \note Le fichier doit être créé à l'aide de la routine 
 *  sauvegarder_arbre_binaire
 */
arbre_binaire* charger_arbre_binaire(char *nom_fichier);

/**
 *  \brief Effectue la désallocation d'un arbre_binaire.
 *
 *  \param arbre l'arbre à désallouer
 */
void detruire_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Sauvegarde l'arbre binaire dans un fichier.
 *
 *  Sauvegarde le contenue de l'arbre dans le fichier 'nom_fichier'.
 *  Si le fichier n'est pas valide, indique l'erreur dans 'arbre->erreur'.
 *
 *  \param arbre L'arbre binaire
 *  \param nom_fichier Le nom du fichier dans lequel on sauvegarde l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void sauvegarder_arbre_binaire(arbre_binaire* arbre, char *nom_fichier);

/**
 *  \brief Renvoie le nombre d'éléments que contient l'arbre.
 *  \param arbre L'arbre binaire
 *
 *  \return Le nombre d'éléments dans l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int nombre_elements_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Retourne le nombre de feuilles que contient l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return Le nombre de feuilles dans l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int nombre_feuilles_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Retourne la hauteur de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return La hauteur de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int hauteur_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Retourne l'élément contenu à la racine de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return La valeur de l'élément contenu dans la racine de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int element_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Modifie l'élément à la racine de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *  \param valeur La valeur à mettre dans la racine de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void modifier_element_arbre_binaire(arbre_binaire* arbre, int valeur);

/**
 *  \brief Recherche dans l'arbre' s'il contient la 'valeur' envoyée.
 *
 *  \param arbre L'arbre binaire où on recherche la 'valeur'
 *  \param valeur La valeur à rechercher dans l'arbre
 *
 *  \return true si la valeur se trouve dans l''arbre', sinon false.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
bool contient_element_arbre_binaire(arbre_binaire* arbre, int valeur);

/**
 *  \brief Retourne l'enfant gauche de l'arbre ou 'NULL' si l'arbre n'a
 *         pas d'enfant gauche.
 *
 *  \param arbre L'arbre binaire où on recherche l'enfant gauche
 *
 *  \return l'enfant gauche de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
arbre_binaire* enfant_gauche_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Créé un enfant gauche d'un arbre et place une valeur dans la
 *         racine de cet enfant.
 *
 *  \param arbre L'arbre binaire où on créé un enfant gauche avec une valeur
 *  \param valeur La valeur à mettre dans la racine de l'enfant gauche
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void creer_enfant_gauche_arbre_binaire(arbre_binaire* arbre, int valeur);

/**
 *  \brief Retire l'enfant gauche d'un arbre.
 *
 *  \param arbre L'arbre binaire où on retire l'enfant gauche
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_enfant_gauche_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Retourne l'enfant droit de l'arbre ou si l'arbre ne
 *         contient pas d'enfant droit, NULL.
 *
 *  \param arbre L'arbre binaire où on recherche l'enfant droit
 *
 *  \return l'enfant droit de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
arbre_binaire* enfant_droit_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Créé un enfant droit d'un arbre et place une valeur dans la
 *         racine de cet enfant.
 *
 *  \param arbre L'arbre binaire où on créé un enfant droit avec une valeur
 *  \param valeur La valeur à mettre dans la racine de l'enfant droit
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void creer_enfant_droit_arbre_binaire(arbre_binaire* arbre, int valeur);

/**
 *  \brief Retire l'enfant droit d'un arbre.
 *
 *  \param arbre L'arbre binaire où on retire l'enfant droit
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_enfant_droit_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Indique si l'arbre contient une erreur.
 *
 *  \return true si l'arbre contient une erreur, sinon false
 *
 *  \note 'arbre' ne doit pas être NULL
 */
bool a_erreur_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Indique le texte de l'erreur contenue dans 'arbre'.
 *
 *  Si 'a_erreur_arbre_binaire' indique que l'arbre contient une erreur,
 *  retourne l'erreur qu'il contient. Si 'a_erreur_arbre_binaire' est Faux,
 *  Retourne une chaine vide.
 *
 *  \return Le texte de l'erreur.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
char* erreur_arbre_binaire(arbre_binaire* arbre);

/**
 *  \brief Inscrit l'erreur dans l'arbre.
 *
 *  \param erreur Le texte de l'erreur
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void inscrire_erreur_arbre_binaire(arbre_binaire* arbre, const char* erreur);

/**
 *  \brief Retire tout erreur dans l'arbre.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_erreur_arbre_binaire(arbre_binaire* arbre);

#endif

/* vi: set ts=4 sw=4 expandtab: */
