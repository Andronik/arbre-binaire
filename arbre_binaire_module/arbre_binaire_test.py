#!/usr/bin/env python
# coding=UTF-8

# =============================================================================
# titre           :setup.py
# description     :Testes de 'arbre_binaire_externe'
# author          :Andronikos Karkaselis
# date            :20191128
# version         :1.0
# usage           :python setup.py build
# notes           :
# license         :MIT
# python_version  :3.7.4
# =============================================================================

import arbre_binaire_externe as ab_ext

arbre1 = ab_ext.creer(7)
print("Arbre créé.")
ab_ext.creer_enfant_gauche(arbre1, 13)
ab_ext.creer_enfant_droit(arbre1, 5)
try:
    ab_ext.creer_enfant_droit(arbre1, 70)
except Exception as message:
    print("ERREUR généré avec succès: "+str(message))
try:
	print("Hauteur de l'arbre(1):", ab_ext.hauteur(arbre1))
except Exception as message:
    print("Ne peut calculer la hauteur: "+str(message)+"\n")
ab_ext.sauvegarder(arbre1, "test.bin")
ab_ext.detruire(arbre1)
arbre2 = ab_ext.charger("test.bin")
if arbre2:
    print("arbre(2) chargé.")
try:
    print("Nombre d'éléments:", ab_ext.nombre_elements(arbre2))
except Exception as message:
    print("Ne peut calculer le nombre d'éléments: "+str(message)+"\n")
try:
	print("Nombre de feuilles:", ab_ext.nombre_feuilles(arbre2))
except Exception as message:
    print("Ne peut calculer le nombre de feuilles: "+str(message)+"\n")
try:
	print("Hauteur de l'arbre:", ab_ext.hauteur(arbre2))
except Exception as message:
    print("Ne peut calculer la hauteur: "+str(message)+"\n")
try:
	print("Element:", ab_ext.element(arbre2))
except Exception as message:
    print("Ne peut afficher l'élément: "+str(message)+"\n")
try:
    ab_ext.modifier_element(arbre2, 31)
except Exception as message:
    print("Ne peut modifier l'élément: " + str(message) + "\n")
try:
	print("Element modifié:", ab_ext.element(arbre2))
except Exception as message:
    print("Ne peut afficher l'élément: "+str(message)+"\n")
if (ab_ext.contient_element(arbre2, 31)):
	print("L'arbre contient l'élément '31'")
else:
	print("Une erreur est survenue lors de 'contient élément'. \n")
un_enfant_gauche = ab_ext.enfant_gauche(arbre2)
ab_ext.creer_enfant_gauche(un_enfant_gauche, 3)
print("Enfant créé.")
try:
	print("Hauteur de l'arbre:", ab_ext.hauteur(arbre2))
except Exception as message:
    print("Ne peut calculer la hauteur: "+str(message)+"\n")
un_enfant_droit = ab_ext.enfant_droit(arbre2)
ab_ext.creer_enfant_droit(un_enfant_gauche, 11)
ab_ext.retirer_enfant_gauche(arbre2)
ab_ext.retirer_enfant_droit(arbre2)
print("Enfants retirés.")
try:
	print("Hauteur de l'arbre:", ab_ext.hauteur(arbre2))
except Exception as message:
    print("Ne peut calculer la hauteur: "+str(message)+"\n")

print("\nFin des Testes\n")
