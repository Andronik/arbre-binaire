#!/usr/bin/env python
# coding=UTF-8

# =============================================================================
# titre           :setup.py
# description     :Script de compilation du module 'arbre_binaire_externe'
# author          :Andronikos Karkaselis
# date            :20191128
# version         :1.0
# usage           :python setup.py build
# notes           :
# license         :MIT
# python_version  :3.7.4
# =============================================================================

from distutils.core import setup, Extension

arbre_binaire_externe = Extension('arbre_binaire_externe',
                    include_dirs = ['../arbre_binaire/'],
                    libraries = ['arbre_binaire'],
                    library_dirs = ['../arbre_binaire/bin/Release/'],
                    sources = ['arbre_binaire_module.c'])

setup (name = 'arbre_binaire_externe',
       version = '1.0',
       description = "Arbre binaire implémenté à l'aide de structures liées",
       ext_modules = [arbre_binaire_externe])