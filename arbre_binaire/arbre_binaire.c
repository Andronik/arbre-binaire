/*

    Copyright (c) 2019 Andronikos Karkaselis

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/**
 *  \file arbre_binaire.c
 *
 *  Fichier d'implémentation contenant la structure de donnée 'arbre_binaire'.
 *  Un arbre binaire est un graphe connexe acyclique débutant par une racine 
 *  qui peut avoir jusqu'à maximum deux enfants avec une valeur pour chaque 
 *  "point" de connection. Ces enfants peuvent avoir à leur tour jusqu'à deux
 *  enfants contenant des valeurs et ainsi de suite.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "arbre_binaire.h"

/**
 * \brief Un arbre binaire qui contient une valeur et des enfants.
 */
struct arbre_binaire_struct {
    int valeur;
    struct arbre_binaire_struct* enfant_gauche;
    struct arbre_binaire_struct* enfant_droit;
    bool a_erreur;
    char* erreur;
};

/**
 *  \brief Créé un nouvel arbre binaire.
 *  
 *  \param valeur La valeur à la racine de l'arbre
 *
 *  \return L'arbre binaire
 */
arbre_binaire* creer_arbre_binaire(int valeur)
{
    arbre_binaire* arbre;
    arbre = calloc(1, sizeof(arbre_binaire));
    if (arbre) {
        arbre->valeur = valeur;
        arbre->enfant_gauche = NULL;
        arbre->enfant_droit = NULL;
        arbre->a_erreur = false;
        arbre->erreur = calloc(ERREUR_TAILLE, sizeof(char));
    }
    return arbre;
}

/**
 *  \brief Lire dans un fichier un arbre binaire
 *  
 *  \param arbre L'arbre binaire à lire dans le fichier
 *  \param fichier Le fichier qui contient l'arbre
 */
void lire_fichier(arbre_binaire* arbre, FILE* fichier) {
    int donnees[3];
    fread(donnees, sizeof(int), 3, fichier);
    modifier_element_arbre_binaire(arbre, donnees[1]);
        if (donnees[0]==1) {
            creer_enfant_gauche_arbre_binaire(arbre, 0);
            lire_fichier(arbre->enfant_gauche, fichier);
        }
        if (donnees[2]==1) {
            creer_enfant_droit_arbre_binaire(arbre, 0);
            lire_fichier(arbre->enfant_droit, fichier);
        }
}

/**
 *  \brief Créé un nouvel arbre_binaire contenu dans un fichier.
 *
 *  Créé un nouvel arbre_binaire et charge le contenue du fichier
 *  'nom_fichier' dans l'arbre. Si le fichier n'est pas valide, indique
 *  l'erreur dans 'arbre->erreur'.
 *
 *  \param nom_fichier Le nom du fichier contenant l'arbre
 *
 *  \return L'arbre binaire
 *
 *  \note Le fichier doit être créé à l'aide de la routine 
 *  sauvegarder_arbre_binaire
 */
arbre_binaire* charger_arbre_binaire(char *nom_fichier)
{
    arbre_binaire* arbre = creer_arbre_binaire(0);
    FILE* fichier = fopen(nom_fichier, "rb");
    if (fichier) {
        lire_fichier(arbre, fichier);
        fclose(fichier);
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                "Ne peux ouvrir le fichier en lecture.");
    }
    return arbre;
} 

/**
 *  \brief Effectue la désallocation d'un arbre_binaire.
 *
 *  \param arbre l'arbre à désallouer
 */
void detruire_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    if (arbre->enfant_gauche) {
        detruire_arbre_binaire(arbre->enfant_gauche);
    }
    if (arbre->enfant_droit) {
        detruire_arbre_binaire(arbre->enfant_droit);
    }
    if (arbre->erreur) {
        free(arbre->erreur);
    }
    free(arbre);
}

/**
 *  \brief Écrit dans un fichier un arbre binaire.
 *  
 *  \param arbre L'arbre binaire à écrire dans le fichier
 *  \param fichier Le fichier dans lequel on écrit
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void ecrire_fichier(arbre_binaire* arbre, FILE* fichier) {
    int donnees[3];
    donnees[1] = arbre->valeur;
    if (arbre->enfant_gauche) {
        donnees[0] = 1;
    } else {
        donnees[0] = 0;
    }
    if (arbre->enfant_droit) {
        donnees[2] = 1;
    } else {
        donnees[2] = 0;
    }
    fwrite(donnees, sizeof(int), 3, fichier);
    if (arbre->enfant_gauche) {
        ecrire_fichier(arbre->enfant_gauche, fichier);
    }
    if (arbre->enfant_droit) {
        ecrire_fichier(arbre->enfant_droit, fichier);
    }
}

/**
 *  \brief Sauvegarde l'arbre binaire dans un fichier.
 *
 *  Sauvegarde le contenue de l'arbre dans le fichier 'nom_fichier'.
 *  Si le fichier n'est pas valide, indique l'erreur dans 'arbre->erreur'.
 *
 *  \param arbre L'arbre binaire
 *  \param nom_fichier Le nom du fichier dans lequel on sauvegarde l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void sauvegarder_arbre_binaire(arbre_binaire* arbre, char *nom_fichier)
{
    retirer_erreur_arbre_binaire(arbre);
    FILE* fichier = fopen(nom_fichier, "wb");
    if (fichier) {
        ecrire_fichier(arbre, fichier);
        fclose(fichier);
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                "Ne peux pas ouvrir le fichier en écriture.");
    }
}

/**
 *  \brief Renvoie le nombre d'éléments que contient l'arbre.
 *  \param arbre L'arbre binaire
 *
 *  \return Le nombre d'éléments dans l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int nombre_elements_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    int nombre_elements = 0;
    if (arbre) {
        nombre_elements++;
        if (arbre->enfant_gauche) {
            nombre_elements += 
                nombre_elements_arbre_binaire(arbre->enfant_gauche);
        }
        if (arbre->enfant_droit) {
            nombre_elements +=
                nombre_elements_arbre_binaire(arbre->enfant_droit);
        }
    }
    return nombre_elements;
}

/**
 *  \brief Retourne le nombre de feuilles que contient l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return Le nombre de feuilles dans l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int nombre_feuilles_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    int nombre_feuilles = 0;
    if (arbre) {
        if (arbre->enfant_gauche || arbre->enfant_droit) {
            if (arbre->enfant_gauche) {
                nombre_feuilles +=
                    nombre_feuilles_arbre_binaire(arbre->enfant_gauche);
            }
            if (arbre->enfant_droit) {
                nombre_feuilles +=
                    nombre_feuilles_arbre_binaire(arbre->enfant_droit);
            }
        } else {
            nombre_feuilles = 1;
        }
    }
    return nombre_feuilles;
}

/**
 *  \brief Retourne la hauteur de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return La hauteur de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int hauteur_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    int hauteur_arbre = 0;
    int hauteur_enfant_gauche = 0;
    int hauteur_enfant_droit = 0;
    if (arbre) {
        if (arbre->enfant_gauche) {
            hauteur_enfant_gauche = hauteur_arbre_binaire(arbre->enfant_gauche);
            hauteur_enfant_gauche++;
        }
        if (arbre->enfant_droit) {
            hauteur_enfant_droit = hauteur_arbre_binaire(arbre->enfant_droit);
            hauteur_enfant_droit++;
        }
        if (hauteur_enfant_gauche > hauteur_enfant_droit) {
            hauteur_arbre = hauteur_enfant_gauche;
        } else {
            hauteur_arbre = hauteur_enfant_droit;
        }
    }
    return hauteur_arbre;
}

/**
 *  \brief Retourne l'élément contenu à la racine de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *
 *  \return La valeur de l'élément contenu dans la racine de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
int element_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    return arbre->valeur;
}

/**
 *  \brief Modifie l'élément à la racine de l'arbre.
 *
 *  \param arbre L'arbre binaire
 *  \param valeur La valeur à mettre dans la racine de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void modifier_element_arbre_binaire(arbre_binaire* arbre, int valeur)
{
    retirer_erreur_arbre_binaire(arbre);
    arbre->valeur = valeur;
}

/**
 *  \brief Recherche dans l'arbre' s'il contient la 'valeur' envoyée.
 *
 *  \param arbre L'arbre binaire où on recherche la 'valeur'
 *  \param valeur La valeur à rechercher dans l'arbre
 *
 *  \return true si la valeur se trouve dans l''arbre', sinon false.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
bool contient_element_arbre_binaire(arbre_binaire* arbre, int valeur)
{
    bool contient_element = false;
    retirer_erreur_arbre_binaire(arbre);
    if (arbre->valeur == valeur) {
        contient_element = true;
    } else {
        if (arbre->enfant_gauche) {
            contient_element = 
                contient_element_arbre_binaire(arbre->enfant_gauche, valeur);
        }
        if (arbre->enfant_droit) {
            contient_element =
                contient_element_arbre_binaire(arbre->enfant_droit, valeur);
        }
    }
    return contient_element;
}

/**
 *  \brief Retourne l'enfant gauche de l'arbre ou 'NULL' si l'arbre n'a
 *         pas d'enfant gauche.
 *
 *  \param arbre L'arbre binaire où on recherche l'enfant gauche
 *
 *  \return l'enfant gauche de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
arbre_binaire* enfant_gauche_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    arbre_binaire* arbre_enfant_gauche = NULL;
    if (arbre->enfant_gauche) {
        arbre_enfant_gauche = arbre->enfant_gauche;
    }
    return arbre_enfant_gauche;
}

/**
 *  \brief Créé un enfant gauche d'un arbre et place une valeur dans la
 *         racine de cet enfant.
 *
 *  \param arbre L'arbre binaire où on créé un enfant gauche avec une valeur
 *  \param valeur La valeur à mettre dans la racine de l'enfant gauche
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void creer_enfant_gauche_arbre_binaire(arbre_binaire* arbre, int valeur)
{
    retirer_erreur_arbre_binaire(arbre);
    if (!arbre->enfant_gauche) {
        arbre_binaire* enfant_gauche = creer_arbre_binaire(valeur);
        arbre->enfant_gauche = enfant_gauche;
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                "L'arbre contient déjà un enfant gauche.");
    }
}

/**
 *  \brief Retire l'enfant gauche d'un arbre.
 *
 *  \param arbre L'arbre binaire où on retire l'enfant gauche
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_enfant_gauche_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    if (arbre->enfant_gauche) {
        detruire_arbre_binaire(arbre->enfant_gauche);
        arbre->enfant_gauche = NULL;
    } else {
        inscrire_erreur_arbre_binaire(arbre,
                "L'arbre ne contient pas d'enfant gauche.");
    }
}

/**
 *  \brief Retourne l'enfant droit de l'arbre ou si l'arbre ne
 *         contient pas d'enfant droit, NULL.
 *
 *  \param arbre L'arbre binaire où on recherche l'enfant droit
 *
 *  \return l'enfant droit de l'arbre
 *
 *  \note 'arbre' ne doit pas être NULL
 */
arbre_binaire* enfant_droit_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    arbre_binaire* arbre_enfant_droit = NULL;
    if (arbre->enfant_droit) {
        arbre_enfant_droit = arbre->enfant_droit;
    }
    return arbre_enfant_droit;
}

/**
 *  \brief Créé un enfant droit d'un arbre et place une valeur dans la
 *         racine de cet enfant.
 *
 *  \param arbre L'arbre binaire où on créé un enfant droit avec une valeur
 *  \param valeur La valeur à mettre dans la racine de l'enfant droit
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void creer_enfant_droit_arbre_binaire(arbre_binaire* arbre, int valeur)
{
    retirer_erreur_arbre_binaire(arbre);   
    if (!arbre->enfant_droit) {
        arbre_binaire* enfant_droit = creer_arbre_binaire(valeur);
        arbre->enfant_droit = enfant_droit;
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                "L'arbre contient déjà un enfant droit.");
    }
}

/**
 *  \brief Retire l'enfant droit d'un arbre.
 *
 *  \param arbre L'arbre binaire où on retire l'enfant droit
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_enfant_droit_arbre_binaire(arbre_binaire* arbre)
{
    retirer_erreur_arbre_binaire(arbre);
    if (arbre->enfant_droit) {
        detruire_arbre_binaire(arbre->enfant_droit);
        arbre->enfant_droit = NULL;
    } else {
        inscrire_erreur_arbre_binaire(arbre,
                "L'abre ne contient pas d'enfant droit.");
    }
}

/**
 *  \brief Indique si l'arbre contient une erreur.
 *
 *  \return true si l'arbre contient une erreur, sinon false
 *
 *  \note 'arbre' ne doit pas être NULL
 */
bool a_erreur_arbre_binaire(arbre_binaire* arbre)
{
    bool erreur = false; 
    if (arbre->a_erreur) {
        erreur = arbre->a_erreur;
    }
    if (!erreur) {
        if (arbre->enfant_gauche) {
            if (a_erreur_arbre_binaire(arbre->enfant_gauche)) {
            erreur = true;
            }
        }
        if (arbre->enfant_droit) {
            if (a_erreur_arbre_binaire(arbre->enfant_droit)) {
            erreur = true;
            }
        }
    }
    return erreur;
}

/**
 *  \brief Indique le texte de l'erreur contenue dans 'arbre'.
 *
 *  Si 'a_erreur_arbre_binaire' indique que l'arbre contient une erreur,
 *  retourne l'erreur qu'il contient. Si 'a_erreur_arbre_binaire' est Faux,
 *  Retourne une chaine vide.
 *
 *  \return Le texte de l'erreur.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
char* erreur_arbre_binaire(arbre_binaire* arbre) 
{
    char* erreur = NULL;
    if (a_erreur_arbre_binaire(arbre)) {
        if (!arbre->a_erreur) {
            if (arbre->enfant_gauche) {
                if (a_erreur_arbre_binaire(arbre->enfant_gauche)) {
                    erreur = erreur_arbre_binaire(arbre->enfant_gauche);
                }
            }
            if (arbre->enfant_droit) {
                if (a_erreur_arbre_binaire(arbre->enfant_gauche)) {
                    erreur = erreur_arbre_binaire(arbre->enfant_gauche);
                }
            }
        }
    } else {
        strncpy(arbre->erreur, "", ERREUR_TAILLE);
    }
    erreur = arbre->erreur;
    return erreur;
}

/**
 *  \brief Inscrit l'erreur dans l'arbre.
 *
 *  \param erreur Le texte de l'erreur
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void inscrire_erreur_arbre_binaire(arbre_binaire* arbre, const char* erreur)
{
    arbre->a_erreur = true;
    strncpy(arbre->erreur, erreur, ERREUR_TAILLE);
}

/**
 *  \brief Retire tout erreur dans l'arbre.
 *
 *  \note 'arbre' ne doit pas être NULL
 */
void retirer_erreur_arbre_binaire(arbre_binaire* arbre)
{
    arbre->a_erreur = false;
    if (arbre->enfant_gauche) {
        retirer_erreur_arbre_binaire(arbre->enfant_gauche);
    }
    if (arbre->enfant_droit) {
        retirer_erreur_arbre_binaire(arbre->enfant_droit);
    }
}

/* vi: set ts=4 sw=4 expandtab: */
/* Indent style: 1TBS */
